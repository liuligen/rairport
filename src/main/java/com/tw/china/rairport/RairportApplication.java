package com.tw.china.rairport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RairportApplication {

	public static void main(String[] args) {
		SpringApplication.run(RairportApplication.class, args);
	}
}
